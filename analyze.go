package dbconvert

import (
	"context"
	"database/sql"

	_ "github.com/jackc/pgx/stdlib"
	_ "github.com/lib/pq"
)

const (
	select_tables = `SELECT
	tablename
 FROM
	pg_catalog.pg_tables
 WHERE
	schemaname != 'pg_catalog'
 AND schemaname != 'information_schema'`

select_columns = `SELECT
column_name
FROM
information_schema.COLUMNS
WHERE
TABLE_NAME = $1`

)

type Analyzer struct {
	d *sql.DB
}

type Tables []string

type Colums []string

func NewAnalyzer(db *sql.DB) *Analyzer {
	return &Analyzer{
		d: db,
	}
}

func (a *Analyzer) Tables() (Tables, error) {
	res, err := a.d.QueryContext(context.Background(), select_tables)
	if err != nil {
		return nil, err
	}
	var t []string
	for res.Next() {
		var n string
		res.Scan(&n)
		t = append(t, n)
	}
	return t, nil
}

func (a *Analyzer) Table(table string) (Colums, error) {
	res, err := a.d.QueryContext(context.Background(), select_columns, table)
	if err != nil {
		return nil, err
	}
	var t []string
	for res.Next() {
		var n string
		res.Scan(&n)
		t = append(t, n)
	}
	return t, nil
}