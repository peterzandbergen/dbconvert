package dbconvert

import (
	"testing"

	"database/sql"
)

func TestGetTables(t *testing.T) {
	connStr := "user=ebms password=secretPassword dbname=ebms sslmode=disable"
	db, err := sql.Open("postgres", connStr)
	if err != nil {
		t.Errorf("error opening: %s", err.Error())
	}
	a := NewAnalyzer(db)
	n, err := a.Tables()
	if err != nil {
		t.Errorf("error getting tables: %s", err.Error())
	}
	for _, tn := range n {
		t.Logf("tablename: %s", tn)
	}
	db.Close()
}


func TestGetColumns(t *testing.T) {
	connStr := "user=ebms password=secretPassword dbname=ebms sslmode=disable"
	db, err := sql.Open("postgres", connStr)
	if err != nil {
		t.Errorf("error opening: %s", err.Error())
	}
	a := NewAnalyzer(db)
	n, err := a.Table("url")
	if err != nil {
		t.Errorf("error getting columns: %s", err.Error())
	}
	for _, tn := range n {
		t.Logf("tablename: %s", tn)
	}
	db.Close()
}
